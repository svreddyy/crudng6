import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  students: any;
  displayedColumns = ['name', 'stclass', 'city'];
  dataSource = new StudentDataSource(this.api);

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getStudents()
      .subscribe(res => {
        console.log(res);
        this.students = res;
      }, err => {
        console.log(err);
      });
  }
}

export class StudentDataSource extends DataSource<any> {
  constructor(private api: ApiService) {
    super()
  }

  connect() {
    return this.api.getStudents();
  }

  disconnect() {

  }
}
