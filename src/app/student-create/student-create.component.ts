import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-student-create',
  templateUrl: './student-create.component.html',
  styleUrls: ['./student-create.component.css']
})
export class StudentCreateComponent implements OnInit {

  studentForm: FormGroup;
  name:string = '';
  stclass:string = '';
  address:string = '';
  city:string = '';
  country:string = '';

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.studentForm = this.formBuilder.group({
      'name' : [null, Validators.required],
      'stclass' : [null, Validators.required],
      'address' : [null, Validators.required],
      'city' : [null, Validators.required],
      'country' : [null, Validators.required]
    });
  }

  onFormSubmit(form:NgForm) {
    this.api.postStudent(form)
      .subscribe(res => {
          let id = res['_id'];
          this.router.navigate(['/student-details', id]);
        }, (err) => {
          console.log(err);
        });
  }
}
