import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-student-edit',
  templateUrl: './student-edit.component.html',
  styleUrls: ['./student-edit.component.css']
})
export class StudentEditComponent implements OnInit {

  studentForm: FormGroup;
  id:string = '';
  name:string = '';
  stclass:string = '';
  address:string = '';
  city:string = '';
  country:string = '';

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getStudent(this.route.snapshot.params['id']);
    this.studentForm = this.formBuilder.group({
      'name' : [null, Validators.required],
      'stclass' : [null, Validators.required],
      'address' : [null, Validators.required],
      'city' : [null, Validators.required],
      'country' : [null, Validators.required]
    });
  }

  getStudent(id) {
    this.api.getStudent(id).subscribe(data => {
      this.id = data._id;
      this.studentForm.setValue({
        name: data.name,
        stclass: data.stclass,
        country: data.country,
        address: data.address,
        city: data.city
      });
    });
  }

  onFormSubmit(form:NgForm) {
    this.api.updateStudent(this.id, form)
      .subscribe(res => {
          let id = res['_id'];
          this.router.navigate(['/student-details', id]);
        }, (err) => {
          console.log(err);
        }
      );
  }

  studentDetails() {
    this.router.navigate(['/student-details', this.id]);
  }
}
