import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.css']
})
export class StudentDetailComponent implements OnInit {

  student = {};

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router) { }

  ngOnInit() {
    this.getStudentDetails(this.route.snapshot.params['id']);
  }

  getStudentDetails(id) {
    this.api.getStudent(id)
      .subscribe(data => {
        console.log(data);
        this.student = data;
      });
  }

  deleteStudent(id) {
    this.api.deleteStudent(id)
      .subscribe(res => {
          this.router.navigate(['/students']);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
