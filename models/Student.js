var mongoose = require('mongoose');

var StudentSchema = new mongoose.Schema({
  name: String,
  stclass: String,
  address: String,
  city: String,
  country: String,
  updated_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Student', StudentSchema);
